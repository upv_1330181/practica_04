package course.examples.practica_04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityRmillas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_rmillas);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        double grados = Double.parseDouble(message);

        grados = grados/0.62137;

        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText("Km \n"+grados);

        setContentView(textView);

    }
}
