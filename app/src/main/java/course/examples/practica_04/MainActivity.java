package course.examples.practica_04;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public final static String EXTRA_MESSAGE = "";
    TabHost tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        Resources res = getResources();

        tabs = (TabHost) findViewById(R.id.tabHost); //llamamos al Tabhost
        tabs.setup();                                                         //lo activamos

        TabHost.TabSpec tab1 = tabs.newTabSpec("tab1");  //aspectos de cada Tab (pestaña)
        TabHost.TabSpec tab2 = tabs.newTabSpec("tab2");
        TabHost.TabSpec tab3 = tabs.newTabSpec("tab3");

        tab1.setIndicator("Grados");    //qué queremos que aparezca en las pestañas
        tab1.setContent(R.id.tab1); //definimos el id de cada Tab (pestaña)

        tab2.setIndicator("Area");
        tab2.setContent(R.id.tab2);

        tab3.setIndicator("Millas");
        tab3.setContent(R.id.tab3);

        tabs.addTab(tab1); //añadimos los tabs ya programados
        tabs.addTab(tab2);
        tabs.addTab(tab3);
    }

    public void conversionGrados(View view){
        Intent intent = new Intent(this, ActivityRgrados.class);
        EditText val = (EditText) findViewById(R.id.inGradosF);
        String message = val.getText().toString();
        if(!message.isEmpty()){
            intent.putExtra(EXTRA_MESSAGE,message);
            startActivity(intent);
        }
    }

    public void sacarArea(View view){
        Intent intent = new Intent(this, ActivityRarea.class);
        EditText val = (EditText) findViewById(R.id.inAreaC);
        String message = val.getText().toString();
        if(!message.isEmpty()) {
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
        }
    }

    public void conversionMillas(View view){
        Intent intent = new Intent(this, ActivityRmillas.class);
        EditText val = (EditText) findViewById(R.id.inMillas);
        String message = val.getText().toString();
        if(!message.isEmpty()){
            intent.putExtra(EXTRA_MESSAGE,message);
            startActivity(intent);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
