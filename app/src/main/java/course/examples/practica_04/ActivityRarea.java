package course.examples.practica_04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityRarea extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_rarea);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        double grados = Double.parseDouble(message);

        grados *= grados ;

        TextView textView = new TextView(this);
        textView.setTextSize(35);
        textView.setText("El area del cuadrado  es  \n"+grados);

        setContentView(textView);

    }
}
